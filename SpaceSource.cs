﻿/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2018-10-11 18:29:44 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2018-10-11 18:29:44
 * 
 * Serialized class for creating named spatialised audio source for use by SpaceAudioManager
 * 
 */
using UnityEngine;

[System.Serializable]
public class SpaceSource {
	public string name;
	public AudioSource source;
}
