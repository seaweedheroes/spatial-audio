﻿/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2019-02-20 15:45:54 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2019-02-25 22:01:51
 * 
 *  Class manages the spatial audio for localPlayer. It is used by PlayerManager and environment game objects (accessed through singleton GameManager that has reference to localPlayer) to play sounds in space.
 * 
 */
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;

public class SpaceAudioManager : MonoBehaviour {
	public SpaceSource[] channels;
	public SpaceSound[] sounds;
    public SpaceSound[] newsounds;
    public SpaceSound[] randomSounds;
	public SpaceSound[] explosion;
	public SpaceSound[] impact;
    public SpaceSound[] sub;

    public float soundNearDistanceLimit = 75f;
    public float soundFarDistanceLimit = 150;

    public List<Pool> pools;

    public Dictionary<string, Queue<GameObject>> poolDict;

    [System.Serializable]
    public class Pool
    {
        public string name;
        public GameObject source;
        public int size;
    }

	private void Start() {
        poolDict = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.source);
                obj.transform.parent = transform.parent;
                obj.SetActive(false);
                objPool.Enqueue(obj);
            }
            poolDict.Add(pool.name, objPool);
        }
		PlayInChannel("CENTRE", sounds, "BG");
	}


    public GameObject ActivateFromPool(string name, Vector3 position, Quaternion rotation)
    {
        if (poolDict == null || !poolDict.ContainsKey(name))
        {
            return null;
        }

        GameObject objToSetActive = poolDict[name].Dequeue();
        objToSetActive.SetActive(true);
        objToSetActive.transform.position = position;
        objToSetActive.transform.rotation = rotation;
        poolDict[name].Enqueue(objToSetActive);

        return objToSetActive;
    }

    public void PlayImpact(Vector3 position)
    {
        int i = UnityEngine.Random.Range(0, impact.Length);
        PlayInSpace(impact, impact[i].name, position);
    }

    public void PlayExplosion(Vector3 position){
        int i = UnityEngine.Random.Range(0, explosion.Length);
        PlayInSpace(explosion, explosion[i].name, position);
    }

	private IEnumerator PlayRandomSound(){
		yield return new WaitForSeconds(UnityEngine.Random.Range(120, 360));
		int ranIdex = UnityEngine.Random.Range(0, randomSounds.Length);
		PlayInChannel("RANDOM", randomSounds, randomSounds[ranIdex].name);
		StartCoroutine(TurnOffRandomSound(randomSounds[ranIdex].clip.length));
	}

	private IEnumerator TurnOffRandomSound(float clipLength){
		yield return new WaitForSeconds(clipLength);
		StartCoroutine(PlayRandomSound());
	}

    private IEnumerator TurnOffPoolSound(float clipLength, GameObject audioObj)
    {
        yield return new WaitForSeconds(clipLength);
        audioObj.SetActive(false);
    }

    public void PlayInSpace(SpaceSound[] clips, string clip, Vector3 hitPoint)
    {
        GameObject spaceAudioObj = null;
        Vector3 vectToSound = hitPoint - gameObject.transform.position;
        if (vectToSound.magnitude > 0 && vectToSound.magnitude < soundNearDistanceLimit)
        {
            //Use near pool
            spaceAudioObj = ActivateFromPool("NEAR", hitPoint, Quaternion.identity);
        }
        else if (vectToSound.magnitude > 0 && vectToSound.magnitude < soundFarDistanceLimit)
        {
            //Use near pool
            spaceAudioObj = ActivateFromPool("FAR", hitPoint, Quaternion.identity);
        }
        SpaceSound sSound = Array.Find(clips, sound => sound.name == clip);
        if (sSound == null)
        {
            Debug.Log("Space sound is missing " + clip + " is missing!");
            return;
        }
        if (spaceAudioObj != null) {
            AudioSource source = spaceAudioObj.GetComponent<AudioSource>();
            if (source != null) {
                source.clip = sSound.clip;
                source.loop = sSound.loop;
                source.volume = UnityEngine.Random.Range(sSound.volume - 0.5f, sSound.volume + 0.5f);
                source.pitch = UnityEngine.Random.Range(sSound.pitch - 0.5f, sSound.pitch + 0.5f);
                source.Play();
                StartCoroutine(TurnOffPoolSound(sSound.clip.length, spaceAudioObj));
            }
        }
    }

    public void PlayInSpace(SpaceSound clip, Vector3 hitPoint)
    {
        GameObject spaceAudioObj = null;
        Vector3 vectToSound = hitPoint - gameObject.transform.position;
        if (vectToSound.magnitude > 0 && vectToSound.magnitude < soundNearDistanceLimit)
        {
            //Use near pool
            spaceAudioObj = ActivateFromPool("NEAR", hitPoint, Quaternion.identity);
        }
        else if (vectToSound.magnitude > 0 && vectToSound.magnitude < soundFarDistanceLimit)
        {
            //Use near pool
            spaceAudioObj = ActivateFromPool("FAR", hitPoint, Quaternion.identity);
        }

        if (spaceAudioObj != null)
        {
            AudioSource source = spaceAudioObj.GetComponent<AudioSource>();
            if (source != null)
            {
                source.clip = clip.clip;
                source.loop = clip.loop;
                // These sounds are environment, impact and explosion sounds so we add some variation 
                // in volume and pitch
                source.volume = UnityEngine.Random.Range(clip.volume - 0.5f, clip.volume + 0.5f);
                source.pitch = UnityEngine.Random.Range(clip.pitch - 0.5f, clip.pitch + 0.5f);
                source.Play();
                StartCoroutine(TurnOffPoolSound(clip.clip.length, spaceAudioObj));
            }
        }
    }

    public void PlayInChannel(string channel, SpaceSound[] clips, string clip){
		SpaceSource sSource = Array.Find(channels, source => source.name == channel);
		SpaceSound sSound = Array.Find(clips, sound => sound.name == clip);
		if(sSource == null ){ 
			Debug.Log("Space source is missing "+channel+" is missing!");
			return; 
		}
		if(sSound == null ){ 
			Debug.Log("Space sound is missing "+clip+" is missing!");
			return; 
		}
		sSource.source.clip = sSound.clip;
		sSource.source.loop = sSound.loop;
		sSource.source.volume = sSound.volume;
		sSource.source.pitch = sSound.pitch;
		sSource.source.Play();
	}
}
