﻿/*
 * @Author: Hendranus Vermeulen 
 * @Date: 2018-10-11 18:29:44 
 * @Last Modified by: Hendranus Vermeulen
 * @Last Modified time: 2018-10-11 18:29:44
 * 
 *  Serialized class for creating named spacialised sounds for use by SpaceAudioManager
 *  
 */

using UnityEngine;

[System.Serializable]
public class SpaceSound {

	public string name;
	public AudioClip clip;
	[Range(0f, 5f)]
	public float volume = 1;
	[Range(0.1f, 2f)]
	public float pitch = 1;
	public bool loop;
}