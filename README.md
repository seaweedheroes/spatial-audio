# Spatial Audio System #

I developed this Spatial Audio System for [Seaweed Heroes](http://www.seaweedheroes.com/). It uses 5 channels around the player's head for important sounds close to the player and then pools for less important environment, impact and explosion sounds. The SpaceAudioManager manages the spatial audio for localPlayer. It is used by PlayerManager and environment game objects (accessed through a singleton GameManager that has reference to localPlayer).

The screenshot below shows how the SpaceAudioManager, SpaceSounds and SpaceSources are put together in the editor:
![GitHub Logo](/soundManager.png)

I would like to share the prototype and source code with you, but I am protective over the intellectual property I developed and therefore ask that you kindly get in contact to sign a NDA.
